<?php
/**
    Template Name: 首頁
 */

get_header(); ?>


<video autoplay muted loop playsinline class="fix-video">
    <source src="<?php echo get_field('bg_video'); ?>" type="video/mp4">
</video>
<div class="video-mask">
</div>
<header id="section_header">
    <div class="slider-wrap">

    <div class="slider main-slider">
        
        <?php if( have_rows('banner_imgs') ): ?>
		    <?php while( have_rows('banner_imgs') ): the_row(); ?>
        
        
            <div>
            <div class="img-wrap">
                <img src="<?php echo get_sub_field('img')['url']; ?>" alt="<?php echo get_sub_field('img')['alt'] ?>">
            </div>
            </div>
            
            <?php endwhile; ?>
		<?php endif; ?>



        

    </div>
    <div class="main-logo-area">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/LOGO_Afbeatz.png" alt="">
    </div>
    </div>
</header>


<!-- section features -->
<section id="features" data-aos="fade-up">
    <div class="container">
    <div class="row">
        <div class="col-12">
        <div class="section-title">
            <div class="decor-line"></div>
            <div class="img-wrap">
            <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/title-style-02.png" alt="">
            </div>
            <div class="decor-line"></div>
        </div>
        </div>
    </div>
    <div class="row">
        
        
        <!-- 1 -->
        <div class="col-12 col-lg-4 card-col">
            <a target="_blank" href="<?php echo get_field('news1'); ?>">
                <div class="feature-card-wrap">
                    <div class="card-title">
                    <h3><?php echo get_field('news_title1'); ?></h3>
                    </div>

                    <div class="card-img">
                    <div class="img-wrap">
                        <img src="<?php echo get_field('news_img1')['url']; ?>" alt="<?php echo get_field('news_img1')['alt']; ?>">
                    </div>
                    <div class="mask-img">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_L.png" alt="">
                    </div>
                    <div class="mask-img mobile">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_C.png" alt="">
                    </div>
                    </div>

                    <div class="card-text">
                    <div class="text-wrap">
                        <p>
                        <?php echo get_field('news_content1'); ?>
                        
                        
                        </p>
                    </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- 2 -->
        <div class="col-12 col-lg-4 card-col">
            <a target="_blank" href="<?php echo get_field('news2'); ?>">
                <div class="feature-card-wrap">
                    <div class="card-title">
                    <h3><?php echo get_field('news_title2'); ?></h3>
                    </div>

                    <div class="card-img">
                    <div class="img-wrap">
                        <img src="<?php echo get_field('news_img2')['url']; ?>" alt="<?php echo get_field('news_img2')['alt']; ?>">
                    </div>
                    <div class="mask-img">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_C.png" alt="">
                    </div>
                    <div class="mask-img mobile">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_C.png" alt="">
                    </div>
                    </div>

                    <div class="card-text">
                    <div class="text-wrap">
                        <p>
                        <?php echo get_field('news_content2'); ?>
                        
                        
                        </p>
                    </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- 3 -->
        <div class="col-12 col-lg-4 card-col">
            <a target="_blank" href="<?php echo get_field('news3'); ?>">
                <div class="feature-card-wrap">
                    <div class="card-title">
                    <h3><?php echo get_field('news_title3'); ?></h3>
                    </div>

                    <div class="card-img">
                    <div class="img-wrap">
                        <img src="<?php echo get_field('news_img3')['url']; ?>" alt="<?php echo get_field('news_img3')['alt']; ?>">
                    </div>
                    <div class="mask-img">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_R.png" alt="">
                    </div>
                    <div class="mask-img mobile">
                        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/BG_NEWS_C.png" alt="">
                    </div>
                    </div>

                    <div class="card-text">
                    <div class="text-wrap">
                        <p>
                        <?php echo get_field('news_content3'); ?>
                        
                        
                        </p>
                    </div>
                    </div>
                </div>
            </a>
        </div>

        
        

    </div>
    </div>
</section>


<section id="group-description" data-aos="fade-up">
    <!-- <img class="bg-img" src="src/dist/img/BG_ABOUT.png" alt=""> -->
    <div class="container">
    <div class="row">
        <div class="col-12">
        <div class="section-title">
            <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/LOGO_Afbeatz.png" alt="">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        <div class="text-wrap">
            <p class="p-1">
                <?php echo get_field('content_p1', 31); ?>
            </p>
            <p class="p-2">
                <?php echo get_field('content_p2', 31); ?>
            </p>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        <div class="about-btn">
            <a href="<?php echo get_the_permalink(31);?>">
            <button>ABOUT</button>
            </a>
        </div>
        </div>
    </div>
    </div>
</section>

<section id="schedule" data-aos="fade-up">
    <div class="container">
    <div class="row">
        <div class="col-12">
        <div class="section-title">
            <div class="decor-line"></div>
            <div class="img-wrap">
            <img class="sche" src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/title-schedule.png" alt="">
            </div>
            <div class="decor-line"></div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        
        <div class="list-wrap">
            <ul class="lists" id="accordion">

            
            <?php if( have_rows('schedules') ): ?>
		        <?php while( have_rows('schedules') ): the_row(); ?>

            <li style="list-style:none;">
                <div id="heading<?php echo $i; ?>">
                
                <div class="list" data-toggle="collapse" data-target="#collapse<?php echo get_row_index(); ?>" aria-expanded="true" aria-controls="collapse<?php echo get_row_index(); ?>">
                    <div class="date col-12 col-lg-2"><?php echo get_sub_field('date') ?></div>
                    <div class="text col-12 col-lg-8"><?php echo get_sub_field('title') ?></div>
                    <div class="cate col-12 col-lg-2"><span><?php echo get_sub_field('tag') ?></span></div>
                </div>
                
                </div>

                <div id="collapse<?php echo get_row_index(); ?>" class="collapse collapsed" aria-labelledby="heading<?php echo get_row_index(); ?>" data-parent="#accordion">
                <div class="card-body">
                    <?php echo get_sub_field('content') ?>
                </div>
                </div>
            </li>


            <!-- <li class="list">
                <a href="">
                <div class="date col-12 col-lg-2">2018.12.23</div>
                <div class="text col-12 col-lg-8">我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。</div>
                <div class="cate col-12 col-lg-2"><span>compaign</span></div>
                </a>
                
            </li> -->

                <?php endwhile; ?>
            <?php endif; ?>



            </ul>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        <div class="see-more-btn">
            <a target="_blank" href="<?php echo get_field('see_more_link'); ?>">
            <button>SEE MORE</button>
            </a>
        </div>
        </div>
    </div>
    </div>
</section>


<section id="youtube-slider" data-aos="fade-up">
    <div class="container">
    <div class="row">
        <div class="col-12">
        <div class="slider-wrap">
            <div class="slider video-slider">

            <?php if( have_rows('videos') ): ?>
		        <?php while( have_rows('videos') ): the_row(); ?>

                <div class="video-wrap">
                    <!-- <iframe src="<?php //echo ('video')?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
                    <?php echo get_sub_field('video'); ?>
                </div>

                <?php endwhile; ?>
            <?php endif; ?>



            </div>
        </div>
        </div>
    </div>
    </div>
</section>


<section id="members" data-aos="fade-up">
    <div class="container">
    
        
    <div class="row">

        <?php if( have_rows('members', 99) ): ?>
            <?php while( have_rows('members', 99) ): the_row(); ?>

        
        <div class="col-md-3 col-lg-2 col-4">
        <a class="box" data-fancybox="mygallery" title="<div style='text-align:center;font-size:18px;'><?php echo get_sub_field('member_content'); ?></div>" href="<?php echo get_sub_field('member_img')['url']; ?>">
            <div class="member" style="background-image: url('<?php echo get_sub_field('member_img')['url']; ?>')">
            <!-- <img src="https://placekitten.com/g/600/600" alt=""> -->
            </div>
        </a>
        
        </div>


            <?php endwhile; ?>
        <?php endif; ?>

    </div>
        
    
    </div>
</section>


<script>
function obj_hover_rotate($hover_obj, $wrap_obj, $move_obj) {
  $($hover_obj).mousemove(function (ev) {
    var oEvent = ev || event;

    var cardWidth = parseInt($($wrap_obj).css('width'));
    var cardHeight = parseInt($($wrap_obj).css('height'));

    var cardLeft = parseInt($($wrap_obj).offset().left + cardWidth / 2);

    var cardTop = parseInt($($wrap_obj).offset().top + cardHeight / 2);

    var centerDisX = oEvent.clientX - cardLeft;
    var centerDisY = oEvent.clientY - cardTop;

    var degX = Math.abs(centerDisY) / (cardHeight / 2) * 10;
    var degY = Math.abs(centerDisX) / (cardWidth / 2) * 10;

    if (centerDisY < 0 && centerDisX < 0) {
      $($move_obj).css({ 'transform': 'rotateX(' + degX + 'deg) rotateY(-' + degY + 'deg)' });
    }
    if (centerDisY < 0 && centerDisX > 0) {
      $($move_obj).css({ 'transform': 'rotateX(' + degX + 'deg) rotateY(' + degY + 'deg)' });
    }
    if (centerDisY > 0 && centerDisX < 0) {
      $($move_obj).css({ 'transform': 'rotateX(-' + degX + 'deg) rotateY(-' + degY + 'deg)' });
    }
    if (centerDisY > 0 && centerDisX > 0) {
      $($move_obj).css({ 'transform': 'rotateX(-' + degX + 'deg) rotateY(' + degY + 'deg)' });
    }
  });
}

obj_hover_rotate("body", ".main-logo-area", ".main-logo-area img");
</script>



<?php
get_footer();
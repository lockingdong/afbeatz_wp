<?php
/**
    Template Name: 關於
 */

get_header(); ?>

<style>
    body {
        background-color: #59128c;
    }
    .slick-slider {
    /* touch-action: auto;
    -ms-touch-action: auto; */
    }
</style>
<div class="bg-img">
</div>
<section id="about">
    
    <div class="container page-title">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <!-- <img src="src/dist/img/about/Title_about.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container page-content" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <p class="p-1">
                        <?php echo get_field('content_p1'); ?>
                    </p>
                    <p class="p-2">
                        <?php echo get_field('content_p2'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
  
    <div class="members-pc">

        
        <?php if( have_rows('members') ): ?>
            <?php while( have_rows('members') ): the_row(); ?>

            

        <div class="member-wrap">
            <img data-aos="fade-up" slide="<?php echo get_row_index()-1; ?>" class="pic slick-img" src="<?php echo get_sub_field('member_img')['url']; ?>" alt="<?php echo get_sub_field('member_img')['alt']; ?>">
            <div class="profile" data-aos="fade-up">
                <h2><?php echo get_sub_field('member_title'); ?>
                </h2>
                <div class="content">
                    <p>
                    <?php echo wp_trim_words(get_sub_field('member_content'), 200, '...'); ?>
                    </p>
                </div>
            </div>
        </div>
            

            <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <div class="members-mb container">
        

        <?php if( have_rows('members') ): ?>
            <?php while( have_rows('members') ): the_row(); ?>
            <div class="row mb" data-aos="fade-up">
                <div class="col-12 col-md-4 <?php echo (get_row_index()-1)%2?'order-md-8':''; ?>">
                    <div class="pic-wrap">
                        <img class="slick-img" slide="<?php echo (get_row_index()-1);?>" src="<?php echo get_sub_field('member_img')['url']; ?>" alt="<?php echo get_sub_field('member_img')['alt']; ?>">
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="content-wrap" slide="<?php echo (get_row_index()-1);?>">
                        <div class="profile <?php echo (get_row_index()-1)%2?'tr-r':'tr-l'; ?>">
                            <h2><?php echo get_sub_field('member_title'); ?>
                            </h2>
                            <div class="content">
                                <p>
                                <?php echo wp_trim_words(get_sub_field('member_content'), 200, '...'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>






    <div class="light-box">
        <button class="close">✕</button>
        <div class="container light-box-container">

        <?php if( have_rows('members') ): ?>
            <?php while( have_rows('members') ): the_row(); ?>

            <div>
                <div class="row row100vh">
                    <div class="col-12 col-md-4">
                        <div class="pic-wrap">
                            <img class="pic pic<?php echo get_row_index();?>" src="<?php echo get_sub_field('member_img')['url']; ?>" alt="<?php echo get_sub_field('member_img')['alt']; ?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="content-wrap" slide="<?php echo (get_row_index()-1);?>">
                            <div class="profile">
                                <h2><?php echo get_sub_field('member_title'); ?>
                                </h2>
                                <div class="content"> 
                                    <p>
                                    <?php echo get_sub_field('member_content'); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endwhile; ?>
        <?php endif; ?>


        </div>
    </div>
</section>



<?php
get_footer();
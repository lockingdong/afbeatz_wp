<?php
/**
    Template Name: video
 */

get_header(); ?>


<?php 
$json = '[
    {
        "cate_name": "afbeatz",
        "desc": "this is test text1",
        "cate_img": "src/dist/img/movie/TitleAfbeatz.png",
        "width": "180",
        "cate_movies": [
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE"
        ]
    },
    {
        "cate_name": "afbeatz family",
        "desc": "this is test text2",
        "cate_img": "src/dist/img/movie/TitleFami.png",
        "width": "250",
        "cate_movies": [
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE"
        ]
    },
    {
        "cate_name": "vibe",
        "desc": "this is test text3",
        "cate_img": "src/dist/img/movie/TitleVibes.png",
        "width": "120",
        "cate_movies": [
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE"
        ]
    },
    {
        "cate_name": "af kid",
        "desc": "this is test text4",
        "cate_img": "src/dist/img/movie/TitleAfbeatzkids.png",
        "width": "250",
        "cate_movies": [
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE",
            "https://www.youtube.com/embed/3zSmmHoL3VE"
        ]
    }
]';

$objs = json_decode($json);
?>
<style>
body {
    background: rgb(108,77,251);
    background: linear-gradient(135deg, #051e29 0%,#3f2461 100%);
    /* background-image: url("src/dist/img/Bg_mask.png"); */
    /* background-size: 60%; */
    /* padding-bottom: 200px; */
    position: relative;
    background-repeat: no-repeat;
}
body::before {
    content: "";
    display: block;
    position: absolute;
    background-image: url("src/dist/img/rp_mask.png");
    background-size: 10%;
    width: 100%;
    height: calc(100%);
    z-index: -10000;
    opacity: 0.1;
    top: 0;
    left: 0;

}
</style>
<section id="movie">
    <div class="container page-title">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <!-- <img src="src/dist/img/about/Title_about.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="slider-nav-wrap">
                    <div class="movie-slider-nav the-slider">
                        <?php if( have_rows('groups') ): ?>
                            <?php while( have_rows('groups') ): the_row(); ?>
                        <div>
                            
                            <div class="img-wrap">
                                <img src="<?php echo get_sub_field('group_img')['url']; ?>" 
                                    alt="<?php echo get_sub_field('group_img')['alt']; ?>"
                                    
                                >
                            </div>
                        </div>


                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="slider-wrap">
                    <div class="movie-slider the-slider">



                        <?php if( have_rows('groups') ): ?>
                            <?php while( have_rows('groups') ): the_row(); ?>

                        <div>
                            <div class="all-video-wrap">

                                <?php if( have_rows('videos') ): ?>
                                    <?php while( have_rows('videos') ): the_row(); ?>
                                
                                <div class="video-wrap-outer">
                                    <div class="video-wrap">
                                        <div class="spin2">
                                            <svg viewbox="0 0 283.465 283.465">
                                                <circle class="c1" cx="141.733" cy="74.073" r="17.88"></circle>
                                                <path class="c2" d="M106.534,81.245c6.981,6.981,6.986,18.304,0,25.288  c-6.981,6.981-18.302,6.979-25.283-0.002c-6.986-6.984-6.986-18.304-0.005-25.286C88.232,74.261,99.548,74.261,106.534,81.245z"></path>
                                                <path class="c3" d="M74.074,123.849c9.872,0,17.882,8.003,17.882,17.88  c0,9.873-8.01,17.878-17.882,17.875c-9.882,0.002-17.883-8.001-17.883-17.873C56.191,131.852,64.192,123.852,74.074,123.849z"></path>
                                                <path class="c4" d="M81.246,176.925c6.977-6.979,18.302-6.984,25.288,0  c6.981,6.981,6.977,18.306-0.005,25.283c-6.986,6.991-18.307,6.989-25.283,0.009C74.26,195.231,74.26,183.916,81.246,176.925z"></path>
                                                <path class="c5" d="M123.845,209.388c0-9.87,8.005-17.88,17.887-17.883  c9.868,0,17.878,8.01,17.868,17.883c0.01,9.882-8,17.885-17.868,17.885C131.855,227.27,123.854,219.27,123.845,209.388z"></path>
                                                <path class="c6" d="M176.926,202.218c-6.982-6.979-6.982-18.304,0-25.292  c6.981-6.979,18.306-6.977,25.278,0.012c6.996,6.979,6.991,18.302,0.014,25.281C195.232,209.199,183.917,209.199,176.926,202.218z"></path>
                                                <path class="c7" d="M209.386,159.616c-9.868,0-17.878-8.008-17.883-17.885  c0.005-9.875,8.015-17.88,17.883-17.868c9.882-0.01,17.887,7.998,17.887,17.868C227.273,151.606,219.268,159.609,209.386,159.616z"></path>
                                                <path class="c8" d="M202.219,106.538c-6.977,6.979-18.302,6.979-25.292,0  c-6.982-6.989-6.977-18.312,0.009-25.283c6.981-6.991,18.307-6.989,25.283-0.01C209.2,88.227,209.2,99.547,202.219,106.538z"></path>
                                            </svg>
                                        </div>
                                        <!-- <iframe src="<?php //echo $url; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                            <!-- 123 -->
                                        <?php echo get_sub_field('video'); ?>
                                    </div>
                                </div>
                            
                                


                                    <?php endwhile; ?>
                                <?php endif; ?>
                                
                            </div>
                        </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                        
                        
                        
                    </div>

                </div>
            </div>

            <div class="col-12">
                <div class="text-slider-wrap">
                    <div class="text-slider the-slider">
                        <?php if( have_rows('groups') ): ?>
                            <?php while( have_rows('groups') ): the_row(); ?>
                        <div class="text-wrap">
                            <p>
                            <?php echo get_sub_field('group_text'); ?>
                            </p>
                        </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<?php
get_footer();
$(".slider.main-slider").slick({
  arrows: false,
  autoplay: true,
  dots: false,
  fade: true,
  autoplaySpeed: 3000
  //swipe: false

});
//alert();


$(".slider.video-slider").slick({
  arrows: true,
  //autoplay: true,
  dots: false,
  //fade: true,
  autoplaySpeed: 3000,
  prevArrow: "<img style='z-index:1000;' class='a-left control-c prev slick-prev' src='./wp-content/themes/afbeatz/src/dist/img/left-arrow.svg'>",
  nextArrow: "<img style='z-index:1000;' class='a-right control-c next slick-next' src='./wp-content/themes/afbeatz/src/dist/img/right-arrow.svg'>"
});

$("a.nav-link").each(function () {
  //console.log($(this).text())
  var text = $(this).text();
  var first_char = text.charAt(0);
  var last_text = text.substring(1, text.length);
  $(this).html("\n    <span class=\"first_char\">" + first_char + "</span>" + last_text + "\n\n  ");
});

function obj_hover_rotate($hover_obj, $wrap_obj, $move_obj) {
  $($hover_obj).mousemove(function (ev) {
    var oEvent = ev || event;

    var cardWidth = parseInt($($wrap_obj).css('width'));
    var cardHeight = parseInt($($wrap_obj).css('height'));

    var cardLeft = parseInt($($wrap_obj).offset().left + cardWidth / 2);

    var cardTop = parseInt($($wrap_obj).offset().top + cardHeight / 2);

    var centerDisX = oEvent.clientX - cardLeft;
    var centerDisY = oEvent.clientY - cardTop;

    var degX = Math.abs(centerDisY) / (cardHeight / 2) * 10;
    var degY = Math.abs(centerDisX) / (cardWidth / 2) * 10;

    if (centerDisY < 0 && centerDisX < 0) {
      $($move_obj).css({ 'transform': 'rotateX(' + degX + 'deg) rotateY(-' + degY + 'deg)' });
    }
    if (centerDisY < 0 && centerDisX > 0) {
      $($move_obj).css({ 'transform': 'rotateX(' + degX + 'deg) rotateY(' + degY + 'deg)' });
    }
    if (centerDisY > 0 && centerDisX < 0) {
      $($move_obj).css({ 'transform': 'rotateX(-' + degX + 'deg) rotateY(-' + degY + 'deg)' });
    }
    if (centerDisY > 0 && centerDisX > 0) {
      $($move_obj).css({ 'transform': 'rotateX(-' + degX + 'deg) rotateY(' + degY + 'deg)' });
    }
  });
}
//obj_hover_rotate("body", ".main-logo-area", ".main-logo-area img");


$('.product.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  speed: 1000,
  fade: true,
  asNavFor: '.slider-nav',
  prevArrow: "<img class='a-left control-c prev slick-prev' src='./src/dist/img/arrow-left.png'>",
  nextArrow: "<img class='a-right control-c next slick-next' src='./src/dist/img/arrow-right.png'>"
});
$('.product.slider-nav').slick({
  slidesToShow: 5,
  //slidesToScroll: 1,
  asNavFor: '.slider-for',
  //dots: false,
  focusOnSelect: true
});
$(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
$(window).resize(function () {
  setTimeout(function () {
    $(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
  }, 100);
});

//===============================

$("body").css("padding-top", $(".navbar").height() + "px");
//$("header#section_header").css("height", `${$(window).height()-$(".navbar").height()}px`);


//==========================menu click outside================


$('button.navbar-toggler').click(function () {
  //if($(".navbar-collapse").hasClass("show") == true)console.log(1)
  $("header ~ *").on('click', function (e) {
    if ($(".navbar-collapse").hasClass("show") === true) {
      e.preventDefault();
      $(".navbar-collapse").removeClass("show");
    } else {
      return true;
    }
  });
});

//==============================


function scrollIn(obj, aniName, aniTime) {
  $(obj).css({ "opacity": "0" });
  $(window).scroll(function () {
    $(obj).each(function () {
      if ($(window).scrollTop() + $(window).height() >= $(this).offset().top) {
        var self = this;
        setTimeout(function () {
          $(self).addClass("animated " + aniName).css("opacity", "1");
          // var inner_self = self;
          // setTimeout(function(){
          //   $(inner_self).removeClass("animated");
          // }, 1000);
        }, aniTime);
      }
    });
  });
  var time = 0;
  $(obj).each(function () {
    if ($(window).scrollTop() + $(window).height() > $(this).offset().top) {
      var self = this;
      setTimeout(function () {
        $(self).addClass("animated " + aniName).css("opacity", "1");
      }, time);
      time += aniTime;
    }
  });
}

// scrollIn("section, header, footer", "fadeIn", 100)

// scrollIn(".box", 100)


//==============//===============ABOUT==============//==============//
//alert()


$(".slick-img,#about .content-wrap").click(function () {
  var init_slide = parseInt($(this).attr("slide"));
  $(".light-box").css("display", "flex").fadeIn();
  setTimeout(function () {

    $(".light-box-container").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: init_slide,
      prevArrow: "<img class='a-left control-c prev slick-prev' src='/wp-content/themes/afbeatz/src/dist/img/left-arrow.svg'>",
      nextArrow: "<img class='a-right control-c next slick-next' src='/wp-content/themes/afbeatz/src/dist/img/right-arrow.svg'>"
    });
  }, 100);
});

// $(".light-box-container").slick({
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   initialSlide: 4,
//   fade: true,
//   prevArrow: "<img class='a-left control-c prev slick-prev' src='../Afbeatz/src/dist/img/left-arrow.svg'>",
//   nextArrow: "<img class='a-right control-c next slick-next' src='../Afbeatz/src/dist/img/right-arrow.svg'>"
// });

$(function () {
  AOS.init();
});

$(window).on('load', function () {
  AOS.refresh();
});
//$('#yourElement').animateCss('bounce');
$("button").click(function () {
  $("p").toggleClass("main");
});

$(".members-pc img").hover(function () {
  $(this).addClass("animated bounce");
}, function () {
  $(this).removeClass("animated bounce");
});

$(".light-box .close").click(function () {
  $(".light-box").fadeOut();
  $(".light-box-container").slick("unslick");
});

//============FAMILY=============//
$(".stu-wrap").hover(function () {
  $(this).addClass("animated bounce");
}, function () {
  $(this).removeClass("animated bounce");
});

$('[data-paroller-factor]').paroller();
$('.paroller').paroller({
  factor: 0.4,
  type: 'foreground'
});

$('body').scrollspy({ target: '#main-navbar' });

$("[data-fancybox]").each(function () {
  $(this).attr("data-caption", $(this).attr("title"));
});

$(".box").fancybox({
  toolbar: "auto",
  buttons: ["close"]
});

//movie-page


$('.movie-slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.the-slider',
  dots: false,
  centerMode: true,
  centerPadding: '0px',
  focusOnSelect: true,
  arrows: false,
  variableWidth: true,
  responsive: [{
    breakpoint: 768,
    settings: {
      prevArrow: "<img class='a-left control-c prev slick-prev' src='/afbeatz/wp-content/themes/afbeatz/src/dist/img/PicArrowL.png'>",
      nextArrow: "<img class='a-right control-c next slick-next' src='/afbeatz/wp-content/themes/afbeatz/src/dist/img/PicArrowR.png'>",
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: false
      //arrows: true,
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  }]
  //fade: true
});

$('.movie-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  speed: 1000,
  prevArrow: "<img style='z-index:1000;' class='a-left control-c prev slick-prev' src='/wp-content/themes/afbeatz/src/dist/img/movie/PicArrowL.png'>",
  nextArrow: "<img style='z-index:1000;' class='a-right control-c next slick-next' src='/wp-content/themes/afbeatz/src/dist/img/movie/PicArrowR.png'>",
  asNavFor: '.the-slider',
  responsive: [{
    breakpoint: 768,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: false
      //arrows: false,
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  }]
});

$('.text-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  speed: 1000,
  // prevArrow:"<img class='a-left control-c prev slick-prev' src='./src/dist/img/movie/PicArrowL.png'>",
  // nextArrow:"<img class='a-right control-c next slick-next' src='./src/dist/img/movie/PicArrowR.png'>",
  asNavFor: '.the-slider'

});
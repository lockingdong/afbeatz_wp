<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <title>AFBEATZ</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
    <link href="src/dist/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet prefetch" href="src/dist/css/slick.css">
    <link rel="stylesheet prefetch" href="src/dist/css/slick-theme.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
    
  </head>
  <body>
  <nav class="navbar navbar-expand-lg fixed-top">
      <div class="container-fluid">
        <a class="s-item navbar-brand xl-brand" href="index.php">
          <img src="src/dist/img/LOGO_AfbeatzS.png" alt="">
        </a>
        <button class="ml-auto navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="#navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
        </button>
        <div class="navbar-collapse collapse" id="navbarsExample04">
          <ul class="navbar-nav mr-auto">
            <li class="b-item nav-item media-item">
              <div class="nav-link">
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    
                    <div class="fab fa-facebook-f"></div>
                    
                  </a>
                </span>
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    <div class="fab fa-youtube"></div>
                      
                  </a>
                </span>
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    <div class="fab fa-instagram"></div>
                      
                  </a>
                </span>
              </div>
            </li>
          </ul>
          
          <ul class="navbar-nav mx-auto ">
            <!-- <div class="move-box"></div> -->
            <li class="nav-item"><a class="nav-link" href="about.php">ABOUT</a></li>
            <li class="nav-item"><a class="nav-link" href="family.php">FAMILY</a></li>
            <li class="nav-item"><a class="nav-link" href="#">SOUL DANCE</a></li>
            <li class="nav-item"><a class="nav-link" href="soos.php">S.O.O.S TAIWAN</a></li>
            <li class="nav-item"><a class="nav-link" href="movie.php">MOVIE</a></li>

          </ul>

          <a class="b-item navbar-brand ml-auto lg-brand" href="index.php">
            <img src="src/dist/img/LOGO_AfbeatzS.png" alt="">
          </a>
          <ul class="s-item navbar-nav">
            <li class="nav-item s-media-item mx-auto">
              <div class="nav-link">
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    
                    <div class="fab fa-facebook-f"></div>
                    
                  </a>
                </span>
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    <div class="fab fa-youtube"></div>
                      
                  </a>
                </span>
                <span class="media-wrap">
                  <a class="media-btn" href="#">
                    <div class="fab fa-instagram"></div>
                      
                  </a>
                </span>
              </div>
            </li>
          </ul>

        </div>
      </div>
    </nav>
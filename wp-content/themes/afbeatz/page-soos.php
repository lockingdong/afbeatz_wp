<?php
/**
    Template Name: soos
 */

get_header(); ?>
<?php 
$json = '[
    {
        "year": "2013",
        "main_img": "src/dist/img/soos/SOOS01.png",
        "vol_img": "src/dist/img/soos/Vol.1.png",
        "bg_img": "src/dist/img/soos/BgSoos1.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    },
    {
        "year": "2014",
        "main_img": "src/dist/img/soos/SOOS02.png",
        "vol_img": "src/dist/img/soos/Vol.2.png",
        "bg_img": "src/dist/img/soos/BgSoos2.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    },
    {
        "year": "2015",
        "main_img": "src/dist/img/soos/SOOS03.png",
        "vol_img": "src/dist/img/soos/Vol.3.png",
        "bg_img": "src/dist/img/soos/BgSoos3.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    },
    {
        "year": "2016",
        "main_img": "src/dist/img/soos/SOOS04.png",
        "vol_img": "src/dist/img/soos/Vol.4.png",
        "bg_img": "src/dist/img/soos/BgSoos4.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    },
    {
        "year": "2017",
        "main_img": "src/dist/img/soos/SOOS05.png",
        "vol_img": "src/dist/img/soos/Vol.5.png",
        "bg_img": "src/dist/img/soos/BgSoos5.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    },
    {
        "year": "2018",
        "main_img": "src/dist/img/soos/SOOS06.png",
        "vol_img": "src/dist/img/soos/Vol.6.png",
        "bg_img": "src/dist/img/soos/BgSoos6.png",
        "content": "wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>wfiwjwjjwfaj<br>"
    }
]';

?>




<style>
body {
    background: rgb(108,77,251);
    background: linear-gradient(0deg, rgba(108,77,251,1) 0%, rgba(0,19,49,1) 100%);
    /* background-image: url("src/dist/img/Bg_mask.png"); */
    background-size: 60%;
    /* padding-bottom: 200px; */
    position: relative;
}
body::before {
    content: "";
    display: block;
    position: absolute;
    background-image: url("src/dist/img/rp_mask.png");
    background-size: 10%;
    width: 100%;
    height: calc(100% - 60px);
    z-index: -10000;
    opacity: 0.1;

}
</style>


<section id="soos">

    <div class="container page-title" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <!-- <img src="src/dist/img/about/Title_about.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>



    <!-- logo  -->
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/soos/PicSoosLogo.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- data-aos="fade-up" --> 

    <div class="container page-content" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <p class="p-1">
                    <?php echo get_field('content_p1'); ?>
                    </p>
                    <p class="p-2">
                    <?php echo get_field('content_p2'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>




    <?php if( have_rows('acts') ): ?>
        <?php $count = count(get_field('acts')); ?>
        <?php while( have_rows('acts') ): the_row(); ?>


    
    <div class="container history-container">
        <div class="line">
            
            <div class="arr" style="display:<?php echo (get_row_index()-1)==0?'block':'none'; ?>">
                <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/soos/arr.svg" alt="">
            </div>

            <div class="arr arr2" style="display:<?php echo (get_row_index()-1)==$count-1?'block':'none'; ?>">
                <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/soos/arr.svg" alt="">
            </div>
        </div>
        <div class="bg" style="background-image:url(<?php echo get_sub_field('bg_img')['url']; ?>);"></div>
        
        <div class="row" data-aos="fade-up">
            <div class="col-12">
                <div class="year-wrap">
                    <h1 class="year"><?php echo get_sub_field('year'); ?></h1>
                    <div class="line"></div>
                </div>
            </div>
            <div class="col-12 col-lg-6 <?php echo (get_row_index()-1)%2?'order-lg-6':''; ?>">
                <div class="poster-wrap">
                    <img src="<?php echo get_sub_field('poster')['url']; ?>" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-6 content-col">
                <div class="content-wrap">
                    <div class="vol-wrap">
                        <img src="<?php echo get_sub_field('vol_img')['url']; ?>" alt="">
                    </div>
                    <div class="content">
                        <div class="content-inner">
                            <?php echo get_sub_field('content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




        <?php endwhile; ?>
    <?php endif; ?>
    


</section>



<?php
get_footer();
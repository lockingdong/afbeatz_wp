<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="robots" content="noindex">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">


<link rel="icon" type="image/png" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/icon.ico">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
<link href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/all.min.css" rel="stylesheet">
<link rel="stylesheet prefetch" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/slick.css">
<link rel="stylesheet prefetch" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/slick-theme.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:300,400,700&amp;subset=chinese-traditional" rel="stylesheet">
<meta property="og:image" content="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/ogimg.png">
<meta property="og:description" content="Afbeatz的成立在2012年。團員們來自於各大舞團中志同道合的Dancer，成員們對於Soul dance這風格皆有長期的研究與喜好。成員有：JIN/MEGA/TAKAE/YOUEI/CHUMI/DINO/JSUN大家都是在街舞圈打滾多年的佼佼者。而在2013年承辦了來自日本大阪的Style of Old Skool 街舞活動。在2016年組成了Afbeatz Family，持續地將喜愛的Soul dance傳承給下個世代。">


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav class="navbar navbar-expand-lg fixed-top">
	<div class="container-fluid">
	<a class="s-item navbar-brand xl-brand" href="<?php echo get_home_url(); ?>">
		<img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/LOGO_AfbeatzS.png" alt="">
	</a>
	<button class="ml-auto navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="#navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
		<span class="icon-bar top-bar"></span>
		<span class="icon-bar middle-bar"></span>
		<span class="icon-bar bottom-bar"></span>
		<span class="sr-only">Toggle navigation</span>
	</button>
	<div class="navbar-collapse collapse" id="navbarsExample04">
		<ul class="navbar-nav mr-auto">
		<li class="b-item nav-item media-item">
			<div class="nav-link">
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('facebook', 8) ?>">
				
				<div class="fab fa-facebook-f"></div>
				
				</a>
			</span>
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('youtube', 8) ?>">
				<div class="fab fa-youtube"></div>
					
				</a>
			</span>
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('ig', 8) ?>">
				<div class="fab fa-instagram"></div>
					
				</a>
			</span>
			</div>
		</li>
		</ul>
		
		<ul class="navbar-nav mx-auto ">
		<!-- <div class="move-box"></div> -->
		<li class="nav-item"><a class="nav-link" href="<?php echo get_the_permalink(31);?>">ABOUT</a></li>
		<li class="nav-item"><a class="nav-link" href="<?php echo get_the_permalink(99);?>">FAMILY</a></li>
		<li class="nav-item"><a target="_blank" class="nav-link" href="<?php echo get_field('soul_dance', 8);?>">SOUL DANCE</a></li>
		<li class="nav-item"><a class="nav-link" href="<?php echo get_the_permalink(123);?>">S.O.O.S TAIWAN</a></li>
		<li class="nav-item"><a class="nav-link" href="<?php echo get_the_permalink(151);?>">MOVIE</a></li>

		</ul>

		<a class="b-item navbar-brand ml-auto lg-brand" href="<?php echo get_home_url(); ?>">
		<img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/LOGO_AfbeatzS.png" alt="">
		</a>
		<ul class="s-item navbar-nav">
		<li class="nav-item s-media-item mx-auto">
			<div class="nav-link">
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('facebook', 8) ?>">
				
				<div class="fab fa-facebook-f"></div>
				
				</a>
			</span>
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('youtube', 8) ?>">
				<div class="fab fa-youtube"></div>
					
				</a>
			</span>
			<span class="media-wrap">
				<a target="_blank" class="media-btn" href="<?php echo get_field('ig', 8) ?>">
				<div class="fab fa-instagram"></div>
					
				</a>
			</span>
			</div>
		</li>
		</ul>

	</div>
	</div>
</nav>


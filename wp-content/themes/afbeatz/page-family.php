<?php
/**
    Template Name: family
 */

get_header(); ?>
<style>
body {
    background-color: #59128c;
    position: relative;

    
    /* height: 1000px; */
}
.slick-slider {
/* touch-action: auto;
-ms-touch-action: auto; */
}

* {
    /* border: 1px solid; */
}
</style>


<!-- data-paroller-factor="0.9" data-paroller-type="foreground" data-paroller-direction="vertical" -->




<section id="family">

    <div class="family-bg-img">
    </div>
    <div class="spot-light">
    </div>
    <div class="container page-title">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <!-- <img src="src/dist/img/about/Title_about.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>

    <!-- logo  -->
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/LOGO_Afbeatz.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- data-aos="fade-up" --> 

    <div class="container page-content" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <p class="p-1">
                    <?php echo get_field('content_p1'); ?>
                    </p>
                    <p class="p-2">
                    <?php echo get_field('content_p2'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>



    <div class="students">
        <div class="bg-imgs-wrap l1" data-paroller-factor="-0.4" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiL1.png" alt="" class="bg-imgs l1">
        </div>

        <div class="bg-imgs-wrap l2" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiL2.png" alt="" class="bg-imgs l2">
        </div>

        <div class="bg-imgs-wrap l3" data-paroller-factor="0.0" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiL3.png" alt="" class="bg-imgs l3">
        </div>

        <div class="bg-imgs-wrap l4" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiL4.png" alt="" class="bg-imgs l4">
        </div>

        <div class="bg-imgs-wrap l5" data-paroller-factor="0.4" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiL5.png" alt="" class="bg-imgs l5">
        </div>

        <div class="bg-imgs-wrap bg-imgs-wrap c1" data-paroller-factor="-0.3" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiC1.png" alt="" class="bg-imgs c1">
        </div>

        <div class="bg-imgs-wrap bg-imgs-wrap c2" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiC2.png" alt="" class="bg-imgs c2">
        </div>

        <div class="bg-imgs-wrap bg-imgs-wrap c3" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiC3.png" alt="" class="bg-imgs c3">
        </div>

        <div class="bg-imgs-wrap bg-imgs-wrap c4" data-paroller-factor="0.3" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiC4.png" alt="" class="bg-imgs c4">
        </div>

        <div class="bg-imgs-wrap r1" data-paroller-factor="-0.4" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiR1.png" alt="" class="bg-imgs r1">
        </div>

        <div class="bg-imgs-wrap r2" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiR2.png" alt="" class="bg-imgs r2">
        </div>

        <div class="bg-imgs-wrap r3" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiR3.png" alt="" class="bg-imgs r3">
        </div>

        <div class="bg-imgs-wrap r4">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiR4.png" alt="" class="bg-imgs r4">
        </div>

        <div class="bg-imgs-wrap r5" data-paroller-factor="0.4" data-paroller-type="foreground" data-paroller-direction="vertical">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/family/BgFamiR5.png" alt="" class="bg-imgs r5">
        </div>
        <div class="container">
            <div class="stu-row">

                <?php if( have_rows('members') ): ?>
                    <?php while( have_rows('members') ): the_row(); ?>
                <!--   data-aos="fade-up" -->
                <a class="box" data-fancybox="mygallery" title="<div style='text-align:center;font-size:18px;'><?php echo get_sub_field('member_content'); ?></div>" href="<?php echo get_sub_field('member_img')['url']; ?>">
                    <div class="stu-wrap">
                        <div class="pic-wrap"
                             style="background-image:url(<?php echo get_sub_field('member_img')['url']; ?>"
                        >
                            <div class="decor decor1"></div>
                            <div class="decor decor2"></div>
                        </div>
                        <div class="stu-name">
                            <div class="name-wrap">
                                <h2>
                                <?php echo get_sub_field('member_title'); ?>
                                </h2> 
                            </div>
                            
                        </div>
                    </div>
                    
                </a>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
</section>



<?php
get_footer();